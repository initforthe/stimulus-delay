# Stimulus Delay

Stimulus controller to send events to enable delayed actions. Can be used in conjunction with `stimulus-reveal` to change visibility of elements.

## Installation

### With yarn/webpacker

```shell
$ yarn add stimulus-delay
```

### With importmaps (Rails)

```shell
$ bin/importmap pin stimulus-delay
```

## Usage

Register the controller with Stimulus:

```javascript
// application.js
import { Application } from "stimulus";
import DelayController from "stimulus-delay";

const application = Application.start();
application.register("delay", DelayController);
```

Initialize the controller on a container element:

```html
<div
  data-controller="reveal delay"
  data-delay-seconds-value="3"
  data-delay-cleanup-value="true"
  data-action="delay:done->reveal#hide mouseenter->delay#pause mouseleave->delay#resume"
>
  <button type="button" data-action="reveal#hide">Click me</button>
  <div hidden data-reveal>I toggle</div>
</div>
```

### Options

#### Top Level options

##### `data-delay-seconds-value`

The number of seconds the delay should last for

##### `data-delay-manual-start-value`

If the timer should be started manually. This can be used for instance to start after an element has finished transitioning in. You will need to call the `start` method on the controller to start the timer.

### Events

Stimulus Delay emits a number of events during the lifecycle of a transition. They are listed below in the order in which they happen:
x
| Name | Description |
| ---- | ----------- |
| `delay:tick` | Emits on each time tick |
| `delay:done` | Emits at the end of the time period |

## Contributing

Fork the project.

Install dependencies

```shell
$ yarn install
```

Write some tests, and add your feature. Send a PR.
