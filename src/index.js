import { Controller } from 'stimulus'

export default class DelayController extends Controller {
  static values = {
    seconds: Number,
    repeating: Boolean,
    manualStart: Boolean
  }

  initialize() {
    this.remainingMilliseconds = this.secondsValue * 1000
    if (!this.manualStartValue) this.start()
  }

  start() {
    this.resume()
  }

  pause() {
    console.log('paused')
    clearTimeout(this.timer)
    this.timer = null
    this.remainingMilliseconds -= Date.now() - this.startTime
  }

  resume() {
    if (this.timer) return

    this.startTime = Date.now()
    this.timer = (this.repeatingValue ? setInterval : setTimeout)(() => {
      this.dispatch(this.eventName)
    }, this.remainingMilliseconds)
  }

  disconnect() {
    if (this.timer) {
      ; (this.repeatingValue ? clearInterval : clearTimeout)(this.timer)
    }
  }

  get eventName() {
    return this.repeatingValue ? 'tick' : 'done'
  }
}
